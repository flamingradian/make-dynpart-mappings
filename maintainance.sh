#!/bin/sh

# Go into the source directory (location of this file)
cd "$(dirname $0)"

# Initial setup
if [ ! -e 3rdparty/upstream/fs_mgr/liblp/include/liblp/metadata_format.h ]
then
	git clone --separate-git-dir=3rdparty/upstream.git --config core.worktree=../upstream https://android.googlesource.com/platform/system/core.git 3rdparty/upstream
fi

git --git-dir=3rdparty/upstream.git pull

find 3rdparty/upstream \
	! -path 3rdparty/upstream \
	! -path 3rdparty/upstream/fs_mgr \
	! -path 3rdparty/upstream/fs_mgr/liblp \
	! -path 3rdparty/upstream/fs_mgr/liblp/include \
	! -path 3rdparty/upstream/fs_mgr/liblp/include/liblp \
	! -path 3rdparty/upstream/fs_mgr/liblp/include/liblp/metadata_format.h \
	-delete

if ! diff -u 3rdparty/metadata_format.h 3rdparty/upstream/fs_mgr/liblp/include/liblp/metadata_format.h
then
	cp 3rdparty/upstream/fs_mgr/liblp/include/liblp/metadata_format.h 3rdparty/metadata_format.h
	echo Metadata format changed, please review
fi
